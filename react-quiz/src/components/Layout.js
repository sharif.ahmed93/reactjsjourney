import cssClassName from "../styles/Layout.module.css";
import Nav from "./Nav";
export default function Layout({ children }) {
  return (
    <>
      <Nav />
      <main className={cssClassName.main}>
        <div className={cssClassName.container}>{children}</div>
      </main>
    </>
  );
}
