import cssClassName from "../styles/Analysis.module.css";
import Quetion from "./Quetion";
export default function Analysis() {
  return (
    <div className={cssClassName.analysis}>
      <h1>Question Analysis</h1>
      <h4>You answerd 5 out of 10 questions correctly</h4>
      <Quetion />
    </div>
  );
}
