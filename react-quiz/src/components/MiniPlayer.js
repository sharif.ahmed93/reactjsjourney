import playerImage from "../assets/images/3.jpg";
import cssClassName from "../styles/MiniPlayer.module.css";
export default function MiniPlayer() {
  return (
    <div className={`${cssClassName.miniPlayer} ${cssClassName.floatingBtn}`}>
      <span className={`material-icons-outlined ${cssClassName.open}`}>
        {" "}
        play_circle_filled{" "}
      </span>
      <span className={`material-icons-outlined ${cssClassName.close}`}>
        {" "}
        close{" "}
      </span>
      <img src={playerImage} alt="" />
      <p>#23 React Hooks Bangla - React useReducer hook Bangla</p>
    </div>
  );
}
