import signUpImage from "../assets/images/signup.svg";
//import loginImage from "../assets/images/login.svg";
import cssClassName from "../styles/Illustration.module.css";
export default function Illustration() {
  return (
    <div className={cssClassName.illustration}>
      <img src={signUpImage} alt="Signup" />
    </div>
  );
}
