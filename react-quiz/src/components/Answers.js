import CheckBox from "../components/CheckBox";
import cssClassName from "../styles/Answers.module.css";
export default function Answers() {
  return (
    <>
      <div className={cssClassName.answers}>
        <CheckBox className={cssClassName.answer} text="Test Answer" />
      </div>
    </>
  );
}
