import cssClassName from "../styles/Form.module.css";
export default function Form({ children, className, ...rest }) {
  return (
    <form
      className={`${cssClassName} ${cssClassName.form}`}
      action="#"
      {...rest}
    >
      {children}
    </form>
  );
}
