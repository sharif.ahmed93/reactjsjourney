import cssClassName from "../styles/Button.module.css";
export default function Button({ className, children }) {
  return (
    <div className={`${cssClassName.button} ${className}`}>{children}</div>
  );
}
