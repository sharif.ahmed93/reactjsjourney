import cssClassName from "../styles/Quetion.module.css";
import Answers from "./Answers";
export default function Quetion() {
  return (
    <div className={cssClassName.question}>
      <div className={cssClassName.qtitle}>
        <span className="material-icons-outlined"> help_outline </span>
        Here goes the question from Learn with Sumit?
      </div>
      <Answers />
    </div>
  );
}
