import cssClassName from "../styles/ProgressBar.module.css";
import Button from "./Button";
import { Link } from "react-router-dom";
export default function ProgressBar() {
  return (
    <div className={cssClassName.progressBar}>
      <div className={cssClassName.backButton}>
        <span className="material-icons-outlined"> arrow_back </span>
      </div>
      <div className={cssClassName.rangeArea}>
        <div className={cssClassName.tooltip}>24% Cimplete!</div>
        <div className={cssClassName.rangeBody}>
          <div className={cssClassName.progress} style={{ width: "20%" }}></div>
        </div>
      </div>
      <Link to="/result">
        <Button className={cssClassName.next}>
          <span>Next Question</span>
          <span className="material-icons-outlined"> arrow_forward </span>
        </Button>
      </Link>
    </div>
  );
}
