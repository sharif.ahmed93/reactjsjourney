import image from "../assets/images/success.png";
import cssClassName from "../styles/Summary.module.css";
export default function Summary() {
  return (
    <div className={cssClassName.summary}>
      <div className={cssClassName.point}>
        {/* progress bar will be placed here  */}
        <p className={cssClassName.score}>
          Your score is <br />5 out of 10
        </p>
      </div>

      <div claclassNamess={cssClassName.badge}>
        <img src={image} alt="Success" />
      </div>
    </div>
  );
}
